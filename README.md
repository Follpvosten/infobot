# "infobot"
Just sends a specific message when a user joins a particular Telegram group.

Reads `config.json` at start; look at `config.example.json` for the format.

## Building and running
0. Clone the repo
1. Install [rustup](https://rustup.rs/)
   * To set your path with (t)csh: `setenv PATH $HOME/.cargo/bin:$PATH`
2. `rustup toolchain add nightly` (if you didn't choose nightly at install)
3. cd into the repo directory
4. `rustup override set nightly` (again, if you didn't choose nightly when you installed rustup)
5. `cargo build --release` - compile everything and get a binary at `target/release/infobot`
6. `cargo run --release` - run (and compile if needed) the release build

That should be it!

## config.json
This config file is required for running the bot; a short breakdown of the options:

Config fields:

* token: Your Telegram bot API token.
* chats: An Array of ChatConfig

ChatConfig fields:

* chatname: The "username" of the chat. Should be the part after the `/` in the group link.
* message: The message that will be sent when a new user joins the chat.
  `$USER` will be replaced by a mention of the user.
