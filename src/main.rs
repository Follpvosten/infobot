use std::fs;

use failure::Error;

use futures::{IntoFuture, Stream};
use serde_derive::Deserialize;
use tokio_core::reactor::Core;

use telebot::{
    functions::*,
    objects::{Message, User},
    RcBot,
};

#[derive(Deserialize)]
struct ChatConfig {
    chatname: String,
    message: String,
}
#[derive(Deserialize)]
struct Config {
    token: String,
    chats: Vec<ChatConfig>,
}
impl Config {
    fn find(&self, chatname: &str) -> Option<&ChatConfig> {
        self.chats.iter().find(|cc| cc.chatname == chatname)
    }
}
impl std::str::FromStr for Config {
    type Err = serde_json::Error;
    fn from_str(s: &str) -> Result<Config, serde_json::Error> {
        serde_json::from_str(&s)
    }
}

fn main() -> Result<(), Error> {
    println!("Setting up event loop...");
    let mut lp = Core::new().unwrap();

    println!("Reading config.json...");
    let config: Config = fs::read_to_string("config.json")?.parse()?;

    let bot = RcBot::new(lp.handle(), &config.token).update_interval(200);

    loop {
        println!("Setting up stream...");
        let stream = bot
            .get_stream()
            .filter_map(|(bot, update)| {
                // Only handle messages
                let mut msg = update.message?;
                // The chat needs a username
                let chatname = msg.chat.username.as_ref()?;
                // Which needs to be in the list
                let msgtext = config.find(chatname)?.message.as_ref();
                // And we need a new chat member
                let user = msg.new_chat_member.take()?;
                Some((bot, msg, user, msgtext))
            })
            .and_then(|(bot, msg, user, msgtext): (RcBot, Message, User, &str)| {
                // If the user has a username, mention them with an @.
                // Otherwise, use a Markdown link.
                let user_mention = if let Some(name) = user.username {
                    format!("@{}", &name)
                } else {
                    format!("[{}](tg://user?id={})", user.first_name, user.id)
                };
                let text = msgtext.replace("$USER", &user_mention);
                bot.message(msg.chat.id, text)
                    .parse_mode(ParseMode::Markdown)
                    .send()
            });

        println!("Starting event loop...");
        if let Err(e) = lp.run(stream.for_each(|_| Ok(())).into_future()) {
            eprintln!("Event loop shutdown:");
            for (i, cause) in e.iter_causes().enumerate() {
                eprintln!(" => {}: {}", i, cause);
            }
        }
    }
}
